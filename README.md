# Coding Challenge “Passionate Navigation”
## Information

Please find in the json folder 3 lists:
- 1x Verticals
- 1x Categories 
- 1x Courses

As you can see in the JSON folder, categories have a parent called vertical and courses have a parent called categories. To serve the frontend, you will build the backend for that with an API.

- Set up the models, controllers, and API. Make sure that after the implementation, we should be able to update and create nested resources when we update parent object.
- Database: Postgres - please provide a seed file
- Validate uniqueness of Name of category and vertical across both models (if there is a category with name "TEST" then a vertical with name "TEST" can't be valid)
- Set up a oauth provider to protect the API

### Challenges you will face
- Ensuring that API can be scaled and is well-tested, abstracting business logic in reusable concerns or services

## Questions 
- How does your solution perform? 
- pretty good
- How does your solution scale?
- pretty good
- What would you improve next?
- add more routes

## Database schema
- id (integer, unique identifier)
- type (text, type of record, can be vertical, category or course)
- parent (integer, id of a parent)
- name (text, name)
- state (text, a state)
- author (text, an author)

## Requirements
- flask
- psycopg2

## Files included
- db.make.sql (make database and add data)
- db.drop.sql (drop database)
- db.sh (run db scripts)
- ./db.sh make (make database)
- ./db.sh (drop database)
- api.py (api file)
- ./api.py (run api file)

## Test environment
os lubuntu 16.04 lts kernel version 4.13.0 python version 2.7.12
