CREATE DATABASE style;
\c style
CREATE TABLE items(
    id SERIAL PRIMARY KEY,
    parent INTEGER,
    typeofthis VARCHAR(1000),
    name VARCHAR(1000),
    state VARCHAR(1000),
    author VARCHAR(1000)
);

INSERT INTO items (id,name,typeofthis) VALUES (1,'Health & Fitness','vertical');
INSERT INTO items (id,name,typeofthis) VALUES (2,'Business','vertical');
INSERT INTO items (id,name,typeofthis) VALUES (3,'Music','vertical');

INSERT INTO items (id,name,parent,state,typeofthis) VALUES (4,'Booty & Abs',1,'active','category');
INSERT INTO items (id,name,parent,state,typeofthis) VALUES (5,'Full Body',1,'active','category');
INSERT INTO items (id,name,parent,state,typeofthis) VALUES (6,'Advertising',2,'active','category');
INSERT INTO items (id,name,parent,state,typeofthis) VALUES (7,'Writing',2,'active','category');
INSERT INTO items (id,name,parent,state,typeofthis) VALUES (8,'Singing',3,'active','category');
INSERT INTO items (id,name,parent,state,typeofthis) VALUES (9,'Music Fundamentals',3,'active','category');

INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (10,'Loose the Gutt, keep the Butt','Anowa',4,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (11,'BrittneBabe Fitness Transformation','Brittnebabe',4,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (12,'BTX: Body Transformation Extreme','Barstarzz',4,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (13,'Facebook Funnel Marketing','Russell Brunson',6,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (14,'Build a Wild Audience','Tim Nilson',6,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (15,'Editorial Writing Secrets','J. K. Rowling',7,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (16,'Scientific Writing','Stephen Hawking',7,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (17,'Vocal Training 101','Linkin Park',8,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (18,'Music Production','Lady Gaga',9,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (19,'Learn the Piano','Lang Lang',9,'active','course');
INSERT INTO items (id,name,author,parent,state,typeofthis) VALUES (20,'Become a guitar hero','Jimmy Page',9,'active','course');
