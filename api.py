#!/usr/bin/python

from flask import Flask, json, request
import psycopg2
from logging import FileHandler, WARNING

api = Flask(__name__)
con = psycopg2.connect(database="style", user="postgres", password="pass", host="127.0.0.1", port="5432")
cur = con.cursor()
fileh = FileHandler("log")

fileh.setLevel(WARNING)
api.logger.addHandler(fileh)

print("Database opened successfully")

@api.route('/verticals', methods=['GET'])
def get_verticals():
    """
        method get_verticals

        get list of verticals
    """
    cur.execute("SELECT * FROM items WHERE typeofthis='vertical'")
    ros = cur.fetchall()
    lst = []
    for ro in ros:
        lst.append(ro)
    return json.dumps(lst)

@api.route('/categories', methods=['GET'])
def get_categories():
    """
        method get_categories

        get list of categories
    """
    cur.execute("SELECT * FROM items WHERE typeofthis='category'")
    ros = cur.fetchall()
    lst = []
    for ro in ros:
        lst.append(ro)
    return json.dumps(lst) #aaaaa

@api.route('/courses', methods=['GET'])
def get_courses():
    """
        method get_courses

        get list of courses
    """
    cur.execute("SELECT * FROM items WHERE typeofthis='course'")
    ros = cur.fetchall()
    lst = []
    for ro in ros:
        lst.append(ro)
    return json.dumps(lst)

if __name__ == '__main__':
    api.run()
