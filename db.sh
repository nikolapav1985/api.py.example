#!/bin/bash

CMP="make"

if [ "$1" == "$CMP" ];
then sudo -u postgres psql < db.make.sql
else sudo -u postgres psql < db.drop.sql
fi
